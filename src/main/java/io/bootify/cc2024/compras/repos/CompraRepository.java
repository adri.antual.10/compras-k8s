package io.bootify.cc2024.compras.repos;

import io.bootify.cc2024.compras.domain.Compra;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;


public interface CompraRepository extends JpaRepository<Compra, UUID> {

    boolean existsByNameIgnoreCase(String name);

}
