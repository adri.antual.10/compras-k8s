package io.bootify.cc2024.compras.config;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;


@Configuration
@EntityScan("io.bootify.cc2024.compras.domain")
@EnableJpaRepositories("io.bootify.cc2024.compras.repos")
@EnableTransactionManagement
public class DomainConfig {
}
