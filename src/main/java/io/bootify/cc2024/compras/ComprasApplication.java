package io.bootify.cc2024.compras;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class ComprasApplication {

    public static void main(final String[] args) {
        SpringApplication.run(ComprasApplication.class, args);
    }

}
