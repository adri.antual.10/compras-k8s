package io.bootify.cc2024.compras.service;

import io.bootify.cc2024.compras.domain.Compra;
import io.bootify.cc2024.compras.model.CompraDTO;
import io.bootify.cc2024.compras.repos.CompraRepository;
import io.bootify.cc2024.compras.util.NotFoundException;
import java.util.List;
import java.util.UUID;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;


@Service
public class CompraService {

    private final CompraRepository compraRepository;

    public CompraService(final CompraRepository compraRepository) {
        this.compraRepository = compraRepository;
    }

    public List<CompraDTO> findAll() {
        final List<Compra> compras = compraRepository.findAll(Sort.by("id"));
        return compras.stream()
                .map(compra -> mapToDTO(compra, new CompraDTO()))
                .toList();
    }

    public CompraDTO get(final UUID id) {
        return compraRepository.findById(id)
                .map(compra -> mapToDTO(compra, new CompraDTO()))
                .orElseThrow(NotFoundException::new);
    }

    public UUID create(final CompraDTO compraDTO) {
        final Compra compra = new Compra();
        mapToEntity(compraDTO, compra);
        return compraRepository.save(compra).getId();
    }

    public void update(final UUID id, final CompraDTO compraDTO) {
        final Compra compra = compraRepository.findById(id)
                .orElseThrow(NotFoundException::new);
        mapToEntity(compraDTO, compra);
        compraRepository.save(compra);
    }

    public void delete(final UUID id) {
        compraRepository.deleteById(id);
    }

    private CompraDTO mapToDTO(final Compra compra, final CompraDTO compraDTO) {
        compraDTO.setId(compra.getId());
        compraDTO.setName(compra.getName());
        return compraDTO;
    }

    private Compra mapToEntity(final CompraDTO compraDTO, final Compra compra) {
        compra.setName(compraDTO.getName());
        return compra;
    }

    public boolean nameExists(final String name) {
        return compraRepository.existsByNameIgnoreCase(name);
    }

}
