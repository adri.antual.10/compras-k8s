package io.bootify.cc2024.compras.model;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import java.util.UUID;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class CompraDTO {

    private UUID id;

    @NotNull
    @Size(max = 255)
    @CompraNameUnique
    private String name;

}
